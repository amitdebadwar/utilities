import 'hammerjs';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ViewChild } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgxEditorModule } from 'ngx-editor';
import { AceEditorModule } from 'ng2-ace-editor';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateMyNewTechnologiesComponent } from './myUtilities/create-my-new-technologies/create-my-new-technologies.component';
import { MaterialModule } from './MaterialModule';
import { js_beautify } from 'js-beautify';
import { DComponent } from './ownM/d/d.component';
import { SModule } from './ownM/s/s.module';


@NgModule({
  declarations: [
    AppComponent, 
    CreateMyNewTechnologiesComponent, 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MaterialModule,
    NgxEditorModule ,
    HttpClientModule,
    AceEditorModule,
    SModule
     
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
