import { Component, OnInit, ViewChild } from '@angular/core';
import {MatChipInputEvent} from '@angular/material';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { ace } from  'ace-builds/src-min-noconflict/ace'; // Load Ace Editor

// Import initial theme and mode so we don't have to wait for 2 extra HTTP requests
import 'ace-builds/src-min-noconflict/theme-chrome';
import 'ace-builds/src-min-noconflict/mode-javascript'; 
 
 

interface Source{
  id : number;
  text : string;
}

export interface Tag {
  name: string;
}

interface Comment{
  id : number;
  text : string;
}


@Component({
  selector: 'app-create-my-new-technologies',
  templateUrl: './create-my-new-technologies.component.html',
  styleUrls: ['./create-my-new-technologies.component.css']
})
export class CreateMyNewTechnologiesComponent implements OnInit {
  separatorKeysCodes: number[] = [ENTER, COMMA];
  allComments : Comment[] = [];
  allSources : Source[] = [];
  blogs = [];

  constructor() { }

  ngOnInit() {
  
 debugger;
    ace.config.set('basePath', '/ace-builds/src-noconflict');
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/monokai");
    editor.getSession().setMode("ace/mode/javascript");

    ace.config.set('basePath', '/ace-builds/src-noconflict');
//ace.config.set('modePath', '/ace-builds/src-noconflict');
//ace.config.set('themePath', '/ace-builds/src-noconflict');
ace.config.set('basePath', './sai/sai.js');
    this.blogs.push({
      "desc":"sai baba",
      "code": "export class Sai { col : number; }"
    });
  }


   step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  addComment(){
    this.allComments.push({id: 1, text:''});
  }
  
  addSource(){
    this.allSources.push({id: 1, text:''});
  }

   allTags: Tag[] = [
    {name: 'JavaScript'},
    {name: 'Angular 7'},
    {name: 'powershell'},
  ];

  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.allTags.push({name: value.trim()});
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeTag(tag: Tag): void {
    const index = this.allTags.indexOf(tag);

    if (index >= 0) {
      this.allTags.splice(index, 1);
    }
  }


    @ViewChild('editor') editor;
    text: string = "";
    technologyDocumentModes : ['javascript', 'c#','css','typescript','java']
    selectedDocumentMode : string;

    ngAfterViewInit() {
      debugger;      
    }



 options:any = {maxLines: 1000, printMargin: false};
    
    onChange(code) {
        console.log("new code", code);
    }

}