import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateMyNewTechnologiesComponent } from './myUtilities/create-my-new-technologies/create-my-new-technologies.component';
import { DComponent } from './ownM/d/d.component';

const routes: Routes = [
  { path: '', component : CreateMyNewTechnologiesComponent},
  { path: 'd', component : DComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
